from django.test import TestCase
from rest_framework.test import APITestCase
from django.shortcuts import reverse
from api.models import Interactome
import api.biogrid_version_control as biogrid_version_control
import copy
import json
import os
import networkx as nx

GET_INTERACTION_REQUIRED_PARAMS = {
    'organism_id': '3702',
    'protein_id': 'AT5G65210',
    'order': 1,
    'scale': 0.5,
    'threshold': 0.5
}

GET_INTERACTION_OPTIONAL_PARAMS = {
    'weight_map': {} 
}

GET_INTERACTION = {**GET_INTERACTION_REQUIRED_PARAMS, **GET_INTERACTION_OPTIONAL_PARAMS}

class TestInteractomeAPI(APITestCase):      
    
    def test_get_interactions_bad_protein(self):
        modified = copy.deepcopy(GET_INTERACTION_REQUIRED_PARAMS)
        modified['protein_id'] = 'this is definitely not a protein id'
        response = self.client.get(reverse("interactions"), modified, format="json")
        self.assertEqual(response.status_code, 404) 

    def test_get_interactions_one_depth(self):
        modified = copy.deepcopy(GET_INTERACTION_REQUIRED_PARAMS)
        modified['order'] = 1 
        response = self.client.get(reverse("interactions"), modified, format="json")
        decoded = json.loads(response.getvalue().decode('utf-8'))
        self.assertGreater(len(list(decoded['elements']['nodes'])), 1)

    def test_get_interactions_zero_depth(self):
        modified = copy.deepcopy(GET_INTERACTION_REQUIRED_PARAMS)
        modified['order'] = 0
        response = self.client.get(reverse("interactions"), modified, format="json")
        decoded = json.loads(response.getvalue().decode('utf-8'))
        self.assertEqual(len(list(decoded['elements']['nodes'])), 1)

    def test_get_interactions_bad_params(self):
        for key in GET_INTERACTION_REQUIRED_PARAMS.keys():
            modified = copy.deepcopy(GET_INTERACTION_REQUIRED_PARAMS)
            modified[key] = [] # Here we simply use any type that can't be casted to a string or float
            response = self.client.get(reverse("interactions"), modified, format="json")
            if response.status_code != 400:
                print(key)
                self.assertEqual(response.status_code, 400)
        self.assertTrue(True)

    def test_get_interactions_missing_optional_params(self):
        response = self.client.get(
            reverse("interactions"), GET_INTERACTION_REQUIRED_PARAMS, format="json")
        self.assertEqual(response.status_code, 200)

    def test_get_interactions_missing_required_params(self):
        for key in GET_INTERACTION_REQUIRED_PARAMS.keys():
            modified = copy.deepcopy(GET_INTERACTION)
            modified.pop(key)
            response = self.client.get(reverse("interactions"), modified, format="json")
            if response.status_code != 400:
                self.assertEqual(response.status_code, 400)
        self.assertTrue(True)

    def test_get_interactions_good(self):
        response = self.client.get(
            reverse("interactions"), GET_INTERACTION, format="json")
        self.assertEqual(response.status_code, 200)
        
    def test_version_control(self):
        current_version = biogrid_version_control.get_current_version()
        local_version = biogrid_version_control.get_latest_local_version()
        if current_version != local_version:
            zip_filename = biogrid_version_control.download_biogrid_tab2_file(
                current_version)
            unziped_filename = biogrid_version_control.unzip_and_save_biogrid_tab2_file(
                zip_filename)
            biogrid_version_control.create_and_save_pickle_file(
                unziped_filename)


